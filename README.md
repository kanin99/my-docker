# my-docker

#### 介绍
使用docker-compose进行管理镜像和容器，快捷创建和启动环境


#### 安装教程

1. ##### 搭建nginx环境

   ```shell
   #在项目根目录执行
   docker-compose up -d nginx
   ```

   注意：nginx配置文件在nginx/vhosts目录，添加或修改nginx配置文件后需要重启容器

2. ##### 搭建php-fpm

    ```shell
    #在项目根目录执行
    docker-compose up -d php-fpm
    ```

注意：php.ini文件在php-fpm目录，修改php.ini文件后需要重启容器

3. ##### 如何进入容器

	```
	 docker-compose exec {docker-compose.yaml文件里面写的服务名} bash
	```

例子：

进入nginx容器

```shell
λ docker-compose exec nginx bash
bash-5.0#

```

 进入php-fpm容器

```
λ docker-compose exec php-fpm bash
root@ca0d848d2190:/var/www#
```




#### 说明：

nginx容器依赖php-fpm容器。所以仅构建nginx容器的时候会自动构建php-fpm容器

**懒得写ENV文件，需要的人自己写吧**

##### 1.docker-compose.yaml文件：

```yaml
version: "3"

networks:
  backend:
    driver: bridge

services:
  php-fpm:
    container_name: php-fpm7.2
    image: kanin/php-fpm
    build:
      context: ./php-fpm
    volumes:
      - ./php-fpm/php.ini:/usr/local/etc/php/php.ini 
      - ../wwwroot/:/var/www #这里是映射宿主机和容器的代码存放路径   {宿主机路径}:{容器路径}
    expose:
      - 9000
    networks:
      - backend

  nginx:
    container_name: nginx
    image: kanin/nginx
    build:
      context: ./nginx
    ports:
      - "8700:80"
    privileged: true
    volumes:
      - ./nginx/vhosts/:/etc/nginx/vhosts
      - ../wwwroot/:/var/www  #这里是映射宿主机和容器的代码存放路径   {宿主机路径}:{容器路径}
      - ./logs/nginx/:/var/log/nginx
    depends_on:
      - php-fpm
    networks:
      - backend


```

